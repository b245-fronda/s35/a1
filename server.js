const express = require("express");

// Mongoose is a package that allows us to create Schemas to model our data structures and to manipulate our database using different acess methods.
const mongoose = require("mongoose");

const port = 3001;

const app = express();

	// [SECTION] MongoDB connection
		// Syntax:
			// mongoose.connect("mongoDBconnectionString", {options to avoid errors in our connection})
	
	mongoose.connect("mongodb+srv://admin:admin@batch245-fronda.yatagmx.mongodb.net/s35-discussion?retryWrites=true&w=majority",
		{
			// Allows us to avoid any current and future errors while connecting to MongoDB
			useNewUrlParser: true,
			useUnifiedTopology: true
		});

	let db = mongoose.connection;

	// error handling in connecting
	db.on("error", console.error.bind(console, "Connection error"));

	// this will be triggered if the connection is successful.
	db.once("open", () => console.log("We're connected to the cloud database!"));

		// Mongoose Schemas
			// Schemas determine the structure of the documents to be written in the database
			// Schemas act as the blueprint to our data
			// Syntax
				// const schemaName = new mongoose.Schema({keyvaluepairs})
		// taskSchema contains two properties: name & status
		// required is used to specify that a field must not be empty

		// default - is used if a field value is not supplied

		const taskSchema = new mongoose.Schema({
			name: {
				type: String,
				required: [true, "Task name is required!"]
			},
			status: {
				type: String,
				default: "pending"
			}
		});

		const userSchema = new mongoose.Schema({
			username: {
				type: String,
				required: [true, "Username is required!"]
			},
			password: {
				type: String,
				default: "12345678"
			}
		});

		// [SECTION] Models
			// Uses schema to create/instatiate documents/objects that follows our schema structure

			// the variable/object that will be created can be used to run commands with our database

			// Syntax:
				// const variableName = mongoose.model("collectionName", schemaName);

		const Task = mongoose.model("Task", taskSchema);
		const User = mongoose.model("User", userSchema);

	// middlewares
	// Allows the app to read json data
	app.use(express.json());
	// it will allow our app to read data form forms.
	app.use(express.urlencoded({extended: true}));

	// [SECTIONS] Routing
		// Create/add new task
			// 1. Check if the task is existing.
				// if task already exist in the database, we will return a message "The task is already existing!"
				// if the task doesn't exist in the database, we will add it in the database

	app.post("/tasks", (request, response) => {
		let input = request.body

			console.log(input.status);
			if(input.status === undefined) {
				Task.findOne({name: input.name}, (error, result) => {
				console.log(result);

				if(result !== null) {
					return response.send("The task is already existing!");
				} else {
					let newTask = new Task({
						name: input.name
					})

					// save() method will save the object in the collection that the object instatiated
					newTask.save((saveError, savedTask) => {
						if(saveError) {
							return console.log(saveError);
						} else {
							return response.send("New Task created!")
						}
					})
				}
			})
		} else {
			Task.findOne({name: input.name}, (error, result) => {
				console.log(result);

				if(result !== null) {
					return response.send("The task is already existing!");
				} else {
					let newTask = new Task({
						name: input.name,
						status: input.status
					})

					// save() method will save the object in the collection that the object instatiated
					newTask.save((saveError, savedTask) => {
						if(saveError) {
							return console.log(saveError);
						} else {
							return response.send("New Task created!")
						}
					})
				}
			})
		}
	})

	// [SECTION] Retrieving all the tasks
	app.get("/tasks", (request, response) => {
		Task.find({}, (error, result) => {
			if(error) {
				console.log(error);
			} else {
				return response.send(result);
			}
		})
	})

	// [s35 Activity]
	app.post("/signup", (request, response) => {
		let input = request.body
		User.findOne({username: input.username}, (error, result) => {
			if(result !== null){
				return response.send("Username already exists!");
			} else {
				let newUser = new User({
					username: input.username,
					password: input.password
				})

				newUser.save((saveError, savedUser) => {
					if(saveError) {
						return console.log(saveError);
					} else {
						return response.send("New User created!")
					}
				})
			}
		})
	})

app.listen(port, () => console.log(`Server is running at port ${port}!`));